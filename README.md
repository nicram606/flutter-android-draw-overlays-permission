# draw_on_top_permission

Draw on top permission handler for Android.

## Usage

To check if app already have drawing overlay permission, use:

```dart
bool canDrawOverlays = await DrawOnTopPermission.canDrawOverlays;
```


To ask for the drawing overlay permission, use:

```dart
bool permissionGranted = await DrawOnTopPermission.askForPermission();
```
