import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:draw_on_top_permission/draw_on_top_permission.dart';

void main() {
  const MethodChannel channel = MethodChannel('draw_on_top_permission');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await DrawOnTopPermission.platformVersion, '42');
  });
}
