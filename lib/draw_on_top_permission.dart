import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';

class DrawOnTopPermission {
  static const MethodChannel _channel =
      const MethodChannel('draw_on_top_permission');

  static Future<bool> get canDrawOverlays async {
    if (!Platform.isAndroid) {
      return false;
    }

    final bool canDraw =
        await _channel.invokeMethod('getDrawOnTopPermissionStatus');
    return canDraw;
  }

  static Future<bool> askForPermission() async {
    if (!Platform.isAndroid) {
      return false;
    }

    final bool response =
        await _channel.invokeMethod('askForDrawOnTopPermission');
    return response;
  }
}
