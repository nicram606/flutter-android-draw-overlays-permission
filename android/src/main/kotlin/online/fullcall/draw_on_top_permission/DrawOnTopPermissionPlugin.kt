package online.fullcall.draw_on_top_permission

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

/** DrawOnTopPermissionPlugin */
public class DrawOnTopPermissionPlugin: MethodCallHandler,
        io.flutter.plugin.common.PluginRegistry.ActivityResultListener, FlutterPlugin, ActivityAware {
  private val drawOnTopPermissionRequestCode = 999;
  private var currentActivity: Activity? = null
  private var currentRequest: Int = 0
  private val resultMap: HashMap<Int, Result> = HashMap()

  override fun onDetachedFromActivity() {
    currentActivity = null
  }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
    currentActivity = binding.activity
    binding.addActivityResultListener(this)
  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    currentActivity = binding.activity
    binding.addActivityResultListener(this)
  }

  override fun onDetachedFromActivityForConfigChanges() {
    currentActivity = null
  }

  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel

  private lateinit var context: Context;

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    context = flutterPluginBinding.applicationContext;
    channel = MethodChannel(flutterPluginBinding.flutterEngine.dartExecutor, "draw_on_top_permission")
    channel.setMethodCallHandler(this)
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), "draw_on_top_permission")
      channel.setMethodCallHandler(DrawOnTopPermissionPlugin())
    }
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when (call.method) {
      "getDrawOnTopPermissionStatus" -> {
        return result.success(getDrawOnTopPermissionStatus())
      }
      "askForDrawOnTopPermission" -> {
        currentRequest++
        resultMap[currentRequest] = result
        askForDrawOnTopPermission(currentRequest)
        return
      }
      else -> {
        return result.notImplemented()
      }
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  private fun getDrawOnTopPermissionStatus(): Boolean {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
      return true
    }

    return Settings.canDrawOverlays(context)
  }

  private fun askForDrawOnTopPermission(requestNumber: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(context)) {
      //If the draw over permission is not available open the settings screen
      //to grant the permission.
      val intent = Intent(
              Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
              Uri.parse("package:${currentActivity!!.packageName}")
      )
      currentActivity?.startActivityForResult(intent, drawOnTopPermissionRequestCode + requestNumber)
    } else if (
            (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(context))
            || Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
      resultMap[requestNumber]?.success(true)
    }

  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
            (requestCode >= drawOnTopPermissionRequestCode && requestCode <= drawOnTopPermissionRequestCode + currentRequest)) {
      resultMap[requestCode - drawOnTopPermissionRequestCode]?.success(Settings.canDrawOverlays(context))
    }

    return requestCode == drawOnTopPermissionRequestCode
  }
}
