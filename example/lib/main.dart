import 'dart:async';

import 'package:draw_on_top_permission/draw_on_top_permission.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool _canDrawOverlays = false;

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    bool canDrawOverlays;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      canDrawOverlays = await DrawOnTopPermission.canDrawOverlays;
    } catch (e) {
      print('ERORR: ${e.toString()}');
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _canDrawOverlays = canDrawOverlays;
    });
  }

  Future<void> _askForPermission() async {
    bool canDrawOverlays = await DrawOnTopPermission.askForPermission();

    setState(() {
      _canDrawOverlays = canDrawOverlays;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text('Can draw overlays: $_canDrawOverlays\n'),
              if (!_canDrawOverlays)
                FlatButton(
                  child: Text('Ask for permission'),
                  onPressed: _askForPermission,
                )
            ],
          ),
        ),
      ),
    );
  }
}
