#import "DrawOnTopPermissionPlugin.h"
#if __has_include(<draw_on_top_permission/draw_on_top_permission-Swift.h>)
#import <draw_on_top_permission/draw_on_top_permission-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "draw_on_top_permission-Swift.h"
#endif

@implementation DrawOnTopPermissionPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftDrawOnTopPermissionPlugin registerWithRegistrar:registrar];
}
@end
